import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEdit } from '@fortawesome/free-solid-svg-icons'
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";

export default class SubtitleHead extends Component{

    constructor(props) {
        super(props);
        this.state = {
            showModal: false,
            subtitleText: 'ТРАЙБ«КОРПОРАТИВНОЕ ВЗЫСКАНИЕ»',
            valueText: 'ТРАЙБ«КОРПОРАТИВНОЕ ВЗЫСКАНИЕ»',
        }
    }

    closeModal = () => {
        this.setState({showModal: false});
    }

    changeSubtitleText = () => {
        this.setState({ showModal: true });
    }

    handleChange = (event) => {
        this.setState({valueText: event.target.value});
    }

    saveSubtitle = () => {
        this.setState({subtitleText: this.state.valueText});
        this.closeModal();
    }

    render() {
        return (
            <Row>
                <Modal show={this.state.showModal}>
                    <Modal.Header closeButton onClick={this.closeModal}>
                        <Modal.Title>Текст подзаголовка</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form>
                            <Form.Group>
                                <Form.Control type="text" placeholder="Введите текст подзаголовка" value={this.state.valueText} onChange={this.handleChange} />
                            </Form.Group>
                        </Form>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={this.closeModal}>
                            Закрыть
                        </Button>
                        <Button variant="primary" onClick={this.saveSubtitle}>
                            Сохранить
                        </Button>
                    </Modal.Footer>
                </Modal>
                <Col sm={8}>
                    <h5 className="card-subtitle subtitle__custom" style={{
                        marginTop: "35px",
                        marginLeft: "59px",
                        fontFamily: "Roboto",
                        fontSize: "14px",
                        letterSpacing: "3px",
                        color: "#b5b5b7"
                    }}>{this.state.subtitleText}</h5>
                </Col>
                <Col sm={4}>
                    <FontAwesomeIcon onClick={this.changeSubtitleText} style={{ marginTop: "35px", color: "green", fontSize: "20px", cursor: "pointer" }} icon={faEdit} />
                </Col>
            </Row>
        );
    }
}

