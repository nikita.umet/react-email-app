import {Component} from 'react';
import * as React from "react";
import SubtitleHead from "./SubtitleHead";

export default class Letter extends Component {
    render() {
        return (
            <div id="letter" className="container" style={{margin: "100px", paddingTop: "50px"}}>
                <div className="row justify-content-center">
                    <div className="col-7">
                        <div className="card card__custom"
                             style={{
                                 width: "600px",
                                 height: "auto",
                                 borderRadius: "20px",
                                 boxShadow: "0 -4px 6px 0 #cad0d7",
                                 backgroundColor: "#ffffff"
                             }}>
                            <SubtitleHead/>
                            <h3 className="card-title title__custom big_font" style={{
                                marginTop: "9px",
                                marginLeft: "59px",
                                fontFamily: "Roboto",
                                fontWeight: 900,
                                color: "#36385b"
                            }}>Внедрение АС УВЗ</h3>
                            <div className="card-body card-custom">
                                <div className="box__success" style={{
                                    marginLeft: "-30px",
                                    paddingTop: "11px",
                                    paddingBottom: "11px",
                                    borderRadius: "10px",
                                    paddingLeft: "69px",
                                    position: "relative",
                                    backgroundColor: "#01ba60",
                                    color: "#fff",
                                    zIndex: 10
                                }}>
                                    <p className="box-text italic" style={{
                                        fontFamily: "Roboto",
                                        fontSize: "16px",
                                        margin: "5px",
                                        fontStyle: "italic",
                                        letterSpacing: "0.5px"
                                    }}>
                                        Уважаемые коллеги!</p>
                                    <p className="box-text italic" style={{
                                        fontFamily: "Roboto",
                                        fontSize: "16px",
                                        margin: "5px",
                                        fontStyle: "italic",
                                        letterSpacing: " 0.5px"
                                    }}>
                                        Сообщаем о внедрении нового релиза АС УВЗ.
                                    </p>
                                </div>
                                <div className="box__success-shadow" style={{
                                    position: "relative",
                                    left: "-578px",
                                    bottom: "3px",
                                    border: "14px solid transparent",
                                    borderRight: "7px solid #36385b",
                                    borderTop: "0px solid #36385b",
                                    padding: "0px"
                                }}/>
                                <h3 className="card-title title__custom medium_font" style={{
                                    marginTop: "9px",
                                    marginLeft: "44px",
                                    fontFamily: "Roboto",
                                    fontWeight: 900,
                                    color: "#36385b",
                                    height: "19px",
                                    fontSize: "16px",
                                    letterSpacing: ".3px"
                                }}>
                                    Реализован следующий бизнес-функционал
                                </h3>
                                <div className="check-with-text d-flex flex-row align-items-start"
                                     style={{margin: "14px 0px 0px 8px"}}>
                                    <img alt="" className="check" src="images/check.svg"
                                         style={{marginTop: "7px", marginRight: "21px"}}/>
                                    <p className="text-for-check" style={{
                                        fontWeight: 400,
                                        fontFamily: "Roboto",
                                        fontSize: "16px",
                                        color: "#36385b",
                                        margin: "0px"
                                    }}>
                                        Проведен рефакторинг функционала формирования и согласования плана работы с
                                        проблемным
                                        активом. План работы формируется, направляется на согласование руководителю (по
                                        ИМС)/
                                        утверждается сотрудником ПРПА самостоятельно (по УМС). Есть возможность
                                        отредактировать план
                                        работы, отозвать с согласования. Задачи плана работы отображаются в Корзине
                                        задач сотрудника
                                        ПРПА.
                                    </p>
                                </div>
                                <div className="check-with-text d-flex flex-row align-items-start"
                                     style={{margin: "14px 0px 0px 8px"}}>
                                    <img alt="" className="check" src="images/check.svg"
                                         style={{marginTop: "7px", marginRight: "21px"}}/>
                                    <p className="text-for-check" style={{
                                        fontWeight: 400,
                                        fontFamily: "Roboto",
                                        fontSize: "16px",
                                        color: "#36385b",
                                        margin: "0px"
                                    }}>
                                        Доработан функционал направления запросов в подразделение безопасности –
                                        сотруднику ПБ
                                        приходят уведомления на почту
                                        о поступлении задачи в работу.
                                    </p>
                                </div>
                                <div className="box__info" style={{
                                    paddingLeft: "16px",
                                    paddingTop: "17px",
                                    paddingBottom: "3px",
                                    marginTop: "22px",
                                    marginLeft: "5px",
                                    borderRadius: "8px",
                                    backgroundColor: "#d8e9fa"
                                }}>
                                    <p className="box-text black-text" style={{
                                        fontFamily: "Roboto",
                                        fontSize: "16px",
                                        margin: "5px",
                                        color: "#36385b"
                                    }}>Обучение территориальных банков по использованию указанного функционала будет
                                        проведено 20.08.2019, 9-30 (МСК) в формате ВКС.</p>
                                    <p className="box-text black-text" style={{
                                        fontFamily: "Roboto",
                                        fontSize: "16px",
                                        margin: "5px",
                                        color: "#36385b"
                                    }}>Ссылка на АС УВЗ 2.0. —
                                        <a className="success-text" href="https://sudirall.ca.sbrf.ru/upv"
                                           style={{color: "#08924c"}}>https://sudirall.ca.sbrf.ru/upv/</a>
                                    </p>
                                </div>
                                <div className="box__secondary" style={{
                                    paddingLeft: "16px",
                                    paddingTop: "17px",
                                    paddingBottom: "10px",
                                    marginTop: "22px",
                                    marginLeft: "5px",
                                    borderRadius: "10px",
                                    backgroundColor: "#eeeeee"
                                }}>
                                    <h3 className="card-title box__title medium_font" style={{
                                        fontFamily: "Roboto",
                                        fontWeight: 900,
                                        color: "#36385b",
                                        height: "19px",
                                        fontSize: "16px",
                                        letterSpacing: ".3px",
                                    }}>Поддержка</h3>
                                    <p className="box-text black-text" style={{
                                        fontFamily: "Roboto",
                                        fontSize: "16px",
                                        margin: "5px",
                                        color: "#36385b"
                                    }}>При наличии дефектов в работе системы – в АС ДРУГ
                                        (в строке поиска набрать «УВЗ» и перейти по ссылке: Проблема во взаимодействии с
                                        внешними
                                        системами (Программное обеспечение/ Управленческие системы/ Pega/ Управление
                                        возвратом
                                        задолженности (ФП Индивидуальная система сбора АС Управление возвратом
                                        задолженности)).
                                    </p>
                                    <p className="box-text black-text mt-0 mb-0" style={{
                                        fontFamily: "Roboto",
                                        fontSize: "16px",
                                        margin: "5px",
                                        color: "#36385b"
                                    }}>При наличии вопросов по использованию функционала -</p>
                                    <p className="box-text black-text mt-0 mb-0" style={{
                                        fontFamily: "Roboto",
                                        fontSize: "16px",
                                        margin: "5px",
                                        color: "#36385b"
                                    }}>Побыйвовк Анастасия (тел: 61-195),</p>
                                    <p className="box-text black-text mt-0 mb-0" style={{
                                        fontFamily: "Roboto",
                                        fontSize: "16px",
                                        margin: "5px,",
                                        color: "#36385b"
                                    }}>Обшарова Наталья (тел: 18-248)</p>
                                </div>
                                <div className="d-flex justify-content-between">
                                    <p className="black-text bottom-text" style={{
                                        marginTop: "10px",
                                        marginLeft: "37px",
                                        width: "123px",
                                        height: "38px",
                                        fontFamily: "Roboto",
                                        fontSize: "16px",
                                        fontStyle: "italic",
                                        color: "#36385b",
                                    }}>
                                        С уважением, Команда АС УВЗ</p>
                                    <p className="black-text bottom-text" style={{
                                        marginTop: "20px",
                                        marginLeft: "59px",
                                        width: "123px",
                                        height: "38px",
                                        fontFamily: "Roboto",
                                        fontSize: "16px",
                                        fontStyle: "italic",
                                        color: "#36385b",
                                    }}>
                                        25.07.2019</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

