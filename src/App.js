import React, {Component} from 'react';

import Letter from "./components/Letter";
import htmlToImage from 'html-to-image';
import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";

class App extends Component {

    saveImage = () => {
        let node = document.getElementById('letter');
        htmlToImage.toPng(node, {width: 1000, height: 1500})
            .then(function (dataUrl) {
                // document.getElementById("letter").remove();
                let img = new Image();
                img.src = dataUrl;
                // document.body.appendChild(img);
                require("downloadjs")(dataUrl, 'my-node.png');
            })
            .catch(function (error) {
                console.error('oops, something went wrong!', error);
            });
    }

    render() {
        return (
            <Container>
                <Row className="justify-content-md-center"><Letter/></Row>
                <Row className="justify-content-md-center m-5">
                    <Button onClick={this.saveImage} size="lg" block>Save!</Button>
                </Row>
            </Container>
        );
    }
}

export default App;
